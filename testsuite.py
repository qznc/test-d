import os
from glob import glob
from test.test   import Test, Environment, ensure_dir
from test.steps  import execute, step_execute
from test.checks import check_retcode_zero, check_retcode_nonzero
from test.suite  import add as add_test, set_environment
from plugins.d   import step_compile_d, step_compile_and_link_d

def extract_from_d_file(environment):
	"""The .d files contain some special comments to influence testing"""
	dflags = list()
	for line in open(environment.filename):
		if "EXTRA_SOURCES:" in line:
			i = line.find(":")
			imports = line[i+1:].strip().split()
			prefix = os.path.dirname(environment.filename)
			for im in imports:
				dflags.append(os.path.join(prefix,im))
		elif "REQUIRED_ARGS:" in line:
			i = line.find(":")
			dflags.append(line[i+1:].strip())
		elif "EXECUTE_ARGS:" in line:
			i = line.find(":")
			environment.executionargs = line[i+1:].strip()
	if not getattr(environment, 'dflags', False):
		environment.dflags = " ".join(dflags)
	else:
		environment.dflags += " %s " % (" ".join(dflags))

def before_factory(dflags=""):
	def before(environment):
		environment.executable = environment.builddir + "/" + environment.filename + ".exe"
		extract_from_d_file(environment)
		environment.dflags += " %s" % dflags
		ensure_dir(os.path.dirname(environment.executable))
	return before

for filename in glob("compilable/*.d"):
	test = Test(filename)
	compile = test.add_step(step_compile_d)
	compile.before = before_factory("-Icompilable")
	compile.add_check(check_retcode_zero)
	add_test(test)

for filename in glob("runnable/*.d"):
	test = Test(filename)
	compile = test.add_step(step_compile_and_link_d)
	compile.before = before_factory("-Irunnable")
	compile.add_check(check_retcode_zero)
	execute = test.add_step(step_execute)
	execute.add_check(check_retcode_zero)
	add_test(test)

for filename in glob("fail_compilation/*.d"):
	test = Test(filename)
	compile = test.add_step(step_compile_d)
	compile.before = before_factory("-Ifail_compilation")
	compile.add_check(check_retcode_nonzero)
	add_test(test)

_env = Environment(
	expect_url="fail_expectations.log",
)
set_environment(_env)

